/*
 * LibHang.Buffers - All the WebGL buffer init stuff.
 *
 * Copyright 2013 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

var Buffers = {
	Cube: {
		initBuffers: function(gl) {
			this.vertexPositionBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexPositionBuffer);
			var vertices = [
				// Front face
				0.0, 0.0, 1.0,
				1.0, 0.0, 1.0,
				1.0, 1.0, 1.0,
				0.0, 1.0, 1.0,

				// Back face
				0.0, 0.0, 0.0,
				0.0, 1.0, 0.0,
				1.0, 1.0, 0.0,
				1.0, 0.0, 0.0,

				// Top face
				0.0, 1.0, 0.0,
				0.0, 1.0, 1.0,
				1.0, 1.0, 1.0,
				1.0, 1.0, 0.0,

				// Bottom face
				0.0, 0.0, 0.0,
				1.0, 0.0, 0.0,
				1.0, 0.0, 1.0,
				0.0, 0.0, 1.0,

				// Right face
				1.0, 0.0, 0.0,
				1.0, 1.0, 0.0,
				1.0, 1.0, 1.0,
				1.0, 0.0, 1.0,

				// Left face
				0.0, 0.0, 0.0,
				0.0, 0.0, 1.0,
				0.0, 1.0, 1.0,
				0.0, 1.0, 0.0,
			];
			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
			this.vertexPositionBuffer.itemSize = 3;
			this.vertexPositionBuffer.numItems = 24;

			this.vertexNormalBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexNormalBuffer);
			var vertexNormals = [
				// Front face
				 0.5,  0.5,  1.0,
				 0.5,  0.5,  1.0,
				 0.5,  0.5,  1.0,
				 0.5,  0.5,  1.0,

				// Back face
				 0.5,  0.5, 0.0,
				 0.5,  0.5, 0.0,
				 0.5,  0.5, 0.0,
				 0.5,  0.5, 0.0,

				// Top face
				 0.5,  1.0,  0.5,
				 0.5,  1.0,  0.5,
				 0.5,  1.0,  0.5,
				 0.5,  1.0,  0.5,

				// Bottom face
				 0.5, 0.0,  0.5,
				 0.5, 0.0,  0.5,
				 0.5, 0.0,  0.5,
				 0.5, 0.0,  0.5,

				// Right face
				 1.0,  0.5,  0.5,
				 1.0,  0.5,  0.5,
				 1.0,  0.5,  0.5,
				 1.0,  0.5,  0.5,

				// Left face
				0.0,  0.5,  0.5,
				0.0,  0.5,  0.5,
				0.0,  0.5,  0.5,
				0.0,  0.5,  0.5
			];
			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexNormals), gl.STATIC_DRAW);
			this.vertexNormalBuffer.itemSize = 3;
			this.vertexNormalBuffer.numItems = 24;

			this.vertexTextureCoordBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexTextureCoordBuffer);
			var textureCoords = [
				// Front face
				0.0, 0.0,
				1.0, 0.0,
				1.0, 1.0,
				0.0, 1.0,

				// Back face
				1.0, 0.0,
				1.0, 1.0,
				0.0, 1.0,
				0.0, 0.0,

				// Top face
				0.0, 1.0,
				0.0, 0.0,
				1.0, 0.0,
				1.0, 1.0,

				// Bottom face
				1.0, 1.0,
				0.0, 1.0,
				0.0, 0.0,
				1.0, 0.0,

				// Right face
				1.0, 0.0,
				1.0, 1.0,
				0.0, 1.0,
				0.0, 0.0,

				// Left face
				0.0, 0.0,
				1.0, 0.0,
				1.0, 1.0,
				0.0, 1.0
			];
			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoords), gl.STATIC_DRAW);
			this.vertexTextureCoordBuffer.itemSize = 2;
			this.vertexTextureCoordBuffer.numItems = 24;

			this.vertexIndexBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.vertexIndexBuffer);
			var vertexIndices = [
				0, 1, 2,      0, 2, 3,	// Front face
				4, 5, 6,      4, 6, 7,	// Back face
				8, 9, 10,     8, 10, 11,  // Top face
				12, 13, 14,   12, 14, 15, // Bottom face
				16, 17, 18,   16, 18, 19, // Right face
				20, 21, 22,   20, 22, 23  // Left face
			];
			gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(vertexIndices), gl.STATIC_DRAW);
			this.vertexIndexBuffer.itemSize = 1;
			this.vertexIndexBuffer.numItems = 36;
			
			this.numFaces = 6;
			this.numItemsPerFace = [
				6, 6, 6, 6, 6, 6
			];
			this.idxItemsPerFace = [
				0, 6, 12, 18, 24, 30
			];
		}
	},
	Sky: {
		initBuffers: function(gl) {
			this.vertexPositionBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexPositionBuffer);
			var vertices = [
				0.5,  0.5,  -0.1,
				-0.5, 0.5,  -0.1,
				-0.5, -0.5, -0.1,
				0.5,  -0.5, -0.1
			];
			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
			this.vertexPositionBuffer.itemSize = 3;
			this.vertexPositionBuffer.numItems = 4;
			
			this.vertexNormalBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexNormalBuffer);
			var vertexNormals = [
				0.5,  0.5,  1.0,
				0.5,  0.5,  1.0,
				0.5,  0.5,  1.0,
				0.5,  0.5,  1.0
			];
			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexNormals), gl.STATIC_DRAW);
			this.vertexNormalBuffer.itemSize = 3;
			this.vertexNormalBuffer.numItems = 4;
			
			this.vertexIndexBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.vertexIndexBuffer);
			var vertexIndices = [
				0, 1, 2,      0, 2, 3
			];
			gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(vertexIndices), gl.STATIC_DRAW);
			this.vertexIndexBuffer.itemSize = 1;
			this.vertexIndexBuffer.numItems = 6;

			this.numFaces = 1;
			this.numItemsPerFace = [
				6
			];
			this.idxItemsPerFace = [
				0
			];
		}
	}
}
