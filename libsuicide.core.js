/*
 * LibSuicide.Core - Core functions and classes for LibSuicide.
 *
 * Copyright 2013 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */


// Float x, Float y, Float z
var Vector = function(x, y, z) {
	this.x=x || 0.0;
	this.y=y || 0.0;
	this.z=z || 0.0;
}
	
Vector.prototype.dup = function() {
	return new Vector(this.x, this.y, this.z);
}
Vector.prototype.set = function(x, y, z) {
	this.x=x;
	this.y=y;
	this.z=z;
	return this;
}
Vector.prototype.toArray = function() {
	return [this.x, this.y, this.z];
}

Vector.prototype.add = function(vector) {
	return new Vector(this.x+vector.x, this.y+vector.y, this.z+vector.z);
}

Vector.prototype.build = function(point1, point2) {
	this.x=point2.x-point1.x;
	this.y=point2.y-point1.y;
	this.z=point2.z-point1.z;
	return this;
}

Vector.prototype.scalar = function(vector) {
	return this.x*vector.x + this.y*vector.y + this.z*vector.z;
}

Vector.prototype.length = function() {
	return Math.sqrt(this.x*this.x + this.y*this.y + this.z*this.z);
}

Vector.prototype.rotate = function(angle, axis) {
	if(axis==0) { // x
		return new Vector(this.x, -this.y*Math.cos(angle)-this.z*Math.sin(angle), -this.y*Math.sin(angle)+this.z*Math.cos(angle));
	}
	if(axis==1) { // y
		return new Vector(this.x*Math.cos(angle)-this.z*Math.sin(angle), -this.y, this.x*Math.sin(angle)+this.z*Math.cos(angle));
	}
	if(axis==2) { // z
		return new Vector(this.x*Math.cos(angle)-this.z*Math.sin(angle), -this.x*Math.sin(angle)+this.z*Math.cos(angle), this.z);
	}
}

// Float angle
var Angle = function(angle) {
	this.angle=angle;
}

Angle.prototype.dup = function() {
	return new Angle(this.angle);
}
Angle.prototype.set = function(angle) {
	this.angle=angle;
	return this;
}

Angle.prototype.add = function(angle) {
	return new Angle(this.angle+angle);
}

// Float x, Float y, Float z
var Point = function(x, y, z) {
	this.x=x;
	this.y=y;
	this.z=z;
}
	
Point.prototype.dup = function() {
	return new Point(this.x, this.y, this.z);
}
Point.prototype.set = function(x, y, z) {
	this.x=x;
	this.y=y;
	this.z=z;
	return this;
}
Point.prototype.toArray = function() {
	return [this.x, this.y, this.z];
}

Point.prototype.add = function(vector) {
	return new Point(this.x+vector.x, this.y+vector.y, this.z+vector.z);
}


// Point corner1, Point corner2, Point corner3
var Rect = function(corner1, corner2, corner3) {
	this.corner1=corner1;
	this.corner2=corner2;
	this.corner3=corner3;
}
	
Rect.prototype.containsPoint = function(point) {
	// Not implemented yet.
}
	
Rect.prototype.intersectsRect = function(rect) {
	// Not implemented yet.
}


// Point pos, Vector diag
var Cube = function(pos, diag) {
	this.pos=pos;
	this.diag=diag;
	this.max_pos=new Point(pos.x+diag.x, pos.y+diag.y, pos.z+diag.z);
}
	
Cube.prototype.dup = function() {
	return new Cube(this.pos, this.diag);
}
Cube.prototype.getPos = function() {
	return this.pos.dup();
}
Cube.prototype.setPos = function(pos) {
	this.pos=pos;
	this.setSize(this.diag);
	return this;
}
Cube.prototype.getSize = function() {
	return this.diag.dup();
}
Cube.prototype.setSize = function(diag) {
	this.diag=diag;
	this.max_pos=this.pos.add(diag);
	return this;
}

Cube.prototype.containsPoint = function(point) {
	if(Math.abs(this.pos.x-point.x) + Math.abs(this.max_pos.x-point.x) == Math.abs(this.max_pos.x-this.pos.x)
			&& Math.abs(this.pos.y-point.y) + Math.abs(this.max_pos.y-point.y) == Math.abs(this.max_pos.y-this.pos.y)
			&& Math.abs(this.pos.z-point.z) + Math.abs(this.max_pos.z-point.z) == Math.abs(this.max_pos.z-this.pos.z))
		return true;
	return false;
}

Cube.prototype.intersectsLine = function(p1, p2) {
	if(this.containsPoint(p1) ^ this.containsPoint(p2))
		return true;
	return false;
}
	
Cube.prototype.intersectsRect = function(rect) {
	// Not implemented yet.
}

Cube.prototype.intersectsCube = function(cube) {
	var cubes=new Array(new Array(this, cube),
			     new Array(cube, this));
	var tmp_vector=new Vector(0, 0, 0);
	var tmp_point=new Point(0, 0, 0);
	
	for(var i=0;i<2;i++) {
		var cube1=cubes[i][0];
		var cube2=cubes[i][1];
		if(!cube1.containsCube(cube2) && (cube1.containsPoint(cube2.pos)
				|| cube1.containsPoint(cube2.pos.add(tmp_vector.set(cube2.diag.x, 0, 0)))
				|| cube1.containsPoint(cube2.pos.add(tmp_vector.set(0, cube2.diag.y, 0)))
				|| cube1.containsPoint(cube2.pos.add(tmp_vector.set(0, 0, cube2.diag.z)))
				|| cube1.containsPoint(cube2.pos.add(tmp_vector.set(cube2.diag.x, cube2.diag.y, 0)))
				|| cube1.containsPoint(cube2.pos.add(tmp_vector.set(cube2.diag.x, 0, cube2.diag.z)))
				|| cube1.containsPoint(cube2.pos.add(tmp_vector.set(0, cube2.diag.y, cube2.diag.z)))
				|| cube1.containsPoint(cube2.pos.add(tmp_vector.set(cube2.diag.x, cube2.diag.y, cube2.diag.z)))))
			return true;
		if((Math.min(Math.abs(cube2.pos.x-cube1.pos.x), Math.abs(cube2.pos.x-cube1.max_pos.x)) < Math.abs(cube2.pos.x-cube2.max_pos.x)
					&& Math.min(Math.abs(cube2.max_pos.x-cube1.pos.x), Math.abs(cube2.max_pos.x-cube1.max_pos.x)) < Math.abs(cube2.pos.x-cube2.max_pos.x))
				&& (cube1.containsPoint(tmp_point.set(cube1.pos.x, cube2.pos.y, cube2.pos.z))
					|| cube1.containsPoint(tmp_point.set(cube1.pos.x, cube2.max_pos.y, cube2.pos.z))
					|| cube1.containsPoint(tmp_point.set(cube1.pos.x, cube2.pos.y, cube2.max_pos.z))
					|| cube1.containsPoint(tmp_point.set(cube1.pos.x, cube2.max_pos.y, cube2.max_pos.z))))
			return true;
		if((Math.min(Math.abs(cube2.pos.y-cube1.pos.y), Math.abs(cube2.pos.y-cube1.max_pos.y)) < Math.abs(cube2.pos.y-cube2.max_pos.y)
					&& Math.min(Math.abs(cube2.max_pos.y-cube1.pos.y), Math.abs(cube2.max_pos.y-cube1.max_pos.y)) < Math.abs(cube2.pos.y-cube2.max_pos.y))
				&& (cube1.containsPoint(tmp_point.set(cube2.pos.x, cube1.pos.y, cube2.pos.z))
					|| cube1.containsPoint(tmp_point.set(cube2.max_pos.x, cube1.pos.y, cube2.pos.z))
					|| cube1.containsPoint(tmp_point.set(cube2.pos.x, cube1.pos.y, cube2.max_pos.z))
					|| cube1.containsPoint(tmp_point.set(cube2.max_pos.x, cube1.pos.y, cube2.max_pos.z))))
			return true;
		if((Math.min(Math.abs(cube2.pos.z-cube1.pos.z), Math.abs(cube2.pos.z-cube1.max_pos.z)) < Math.abs(cube2.pos.z-cube2.max_pos.z)
					&& Math.min(Math.abs(cube2.max_pos.z-cube1.pos.z), Math.abs(cube2.max_pos.z-cube1.max_pos.z)) < Math.abs(cube2.pos.z-cube2.max_pos.z))
				&& (cube1.containsPoint(tmp_point.set(cube2.pos.x, cube2.pos.y, cube1.pos.z))
					|| cube1.containsPoint(tmp_point.set(cube2.max_pos.x, cube2.pos.y, cube1.pos.z))
					|| cube1.containsPoint(tmp_point.set(cube2.pos.x, cube2.max_pos.y, cube1.pos.z))
					|| cube1.containsPoint(tmp_point.set(cube2.max_pos.x, cube2.max_pos.y, cube1.pos.z))))
			return true;
	}
	return false;
}

Cube.prototype.containsCube = function(cube) {
	var temp_vector=new Vector(0, 0, 0);
	if(this.containsPoint(cube.pos)
			&& this.containsPoint(cube.pos.add(tmp_vector.set(cube.diag.x, 0, 0)))
			&& this.containsPoint(cube.pos.add(tmp_vector.set(0, cube.diag.y, 0)))
			&& this.containsPoint(cube.pos.add(tmp_vector.set(0, 0, cube.diag.z)))
			&& this.containsPoint(cube.pos.add(tmp_vector.set(cube.diag.x, cube.diag.y, 0)))
			&& this.containsPoint(cube.pos.add(tmp_vector.set(cube.diag.x, 0, cube.diag.z)))
			&& this.containsPoint(cube.pos.add(tmp_vector.set(0, cube.diag.y, cube.diag.z)))
			&& this.containsPoint(cube.pos.add(tmp_vector.set(cube.diag.x, cube.diag.y, cube.diag.z))))
		return true;
	return false;
}


// Point pos, Vector diag, [Bool is_tangible=false]
var FixedEntity = function(pos, diag, is_tangible) {
	this.local_cube=new Cube(pos, diag);
	this.is_tangible=typeof is_tangible !== 'undefined' ? is_tangible : false;
}

FixedEntity.prototype.toMoving = function() {
	return new MovingEntity(this.isTangible(), this.getPos(), this.getSize());
}
	
FixedEntity.prototype.getPos = function() {
	return this.local_cube.getPos();
}
FixedEntity.prototype.setPos = function(pos) {
	this.local_cube.setPos(pos);
	return this;
}
FixedEntity.prototype.getSize = function() {
	return this.local_cube.getSize();
}
FixedEntity.prototype.setSize = function(diag) {
	this.local_cube.setSize(diag);
	return this;
}
FixedEntity.prototype.isTangible = function() {
	return this.is_tangible;
}
FixedEntity.prototype.setTangible = function(is_tangible) {
	this.is_tangible=is_tangible;
	return this;
}


/*
 * is_force_move_allowed (optional, default=true) : The entity can be moved indirectly (collision) by another Entity.
 */
// Point pos, Vector diag, [Bool is_tangible=false], [Bool is_force_move_allowed=true]
var MovableEntity = function(pos, diag, is_tangible, is_force_move_allowed) {
	FixedEntity.call(this, pos, diag, is_tangible);
	this.is_force_move_allowed=typeof is_force_move_allowed !== 'undefined' ? is_force_move_allowed : true;
}
extendClass(MovableEntity, FixedEntity);
	
MovableEntity.prototype.isForceMoveAllowed = function() {
	return this.is_force_move_allowed;
}
MovableEntity.prototype.setForceMoveAllowed = function(is_indirect_move_allowed) {
	this.is_force_move_allowed=is_force_move_allowed;
	return this;
}

MovableEntity.prototype.toFixed = function() {
	return new FixedEntity(this.isTangible(), this.getPos(), this.getSize());
}

// Absolute move.
MovableEntity.prototype.teleport = function(pos, force) {
	this.local_cube.setPos(pos);
	return this;
}

// Vectorial move.
MovableEntity.prototype.move = function(vector) {
	if(typeof func_can_move === 'undefined' || (typeof func_can_move !== 'undefined' && func_can_move(this, vector)))
		this.teleport(this.local_cube.getPos().add(vector));
	return this;
}


// Point pos, [Array entities], [Bool is_tangible=false]
var FixedSuperEntity = function(pos, entities, is_tangible) {
	this.pos=pos;
	this.entities=typeof entities !== 'undefined' ? entities : new Array();
	this.is_tangible=typeof is_tangible !== 'undefined' ? is_tangible : false;
}
	
FixedSuperEntity.prototype.getPos = function() {
	return this.pos.clone();
}
FixedSuperEntity.prototype.setPos = function(pos) {
	this.pos=pos;
	return this;
}
FixedSuperEntity.prototype.isTangible = function() {
	return is_tangible;
}
FixedSuperEntity.prototype.setTangible = function(is_tangible) {
	this.is_tangible=is_tangible;
	return this;
}

FixedSuperEntity.prototype.addEntity = function(entity) {
	this.entities.push(entity);
}


// Point pos, [Array entities], [Bool is_tangible=false], [Bool is_tangible=true]
var MovableSuperEntity = function(pos, entities, is_tangible, is_indirect_move_allowed) {
	FixedSuperEntity.call(this, pos, entities, is_tangible);
	this.is_indirect_move_allowed=typeof is_indirect_move_allowed !== 'undefined' ? is_indirect_move_allowed : true;
}
extendClass(MovableSuperEntity, FixedSuperEntity);

MovableEntity.prototype.isIndirectMoveAllowed = function() {
	return this.is_indirect_move_allowed;
}
MovableEntity.prototype.setIndirectMoveAllowed = function(is_indirect_move_allowed) {
	this.is_indirect_move_allowed=is_indirect_move_allowed;
	return this;
}

// Incremental move.
MovableSuperEntity.prototype.move = function(vector) {
	for(var i=0;i<entities.length;i++) {
		if(entities[i]["move"] === true)
			entities[i].move(vector);
	}
}


// px.s⁻²
var GRAVITY_CONSTANTS = {
	EARTH:9.81,
}


// Point pos, Vector diag, Float gravity
var World = function(pos, diag, gravity) {
	this.area=new Cube(pos, diag);
	this.gravity=gravity;
	this.entities=new Array();
	this.sub_worlds=new Array();
}

World.prototype.getEntity = function(idx) {
	return entities[idx];
}
World.prototype.addEntity = function(entity) {
	idx=this.entities.push(entity)-1;
	return idx;
}
World.prototype.removeEntity = function(idx) {
	this.entities.splice(idx, 1);
}

