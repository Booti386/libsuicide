/*
 * LibHang.Shaders - All the shaders needed by LibHang.
 *
 * Copyright 2013 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

var Shaders = {
	Cube: {
		vs: "precision mediump float; \n\
			 \n\
			attribute vec3 a_VertexPosition; \n\
			attribute vec2 a_TextureCoord; \n\
			 \n\
			uniform mat4 u_MVMatrix; \n\
			uniform mat4 u_PMatrix; \n\
			 \n\
			uniform vec3 u_AmbientLight; \n\
			 \n\
			uniform vec3 u_RelPosition; \n\
			uniform vec3 u_CameraPosition; \n\
			 \n\
			uniform bool u_FogEnabled; \n\
			uniform float u_FogMinDistance; \n\
			uniform float u_FogMaxDistance; \n\
			 \n\
			varying vec2 v_TextureCoord; \n\
			varying vec3 v_LightWeighting; \n\
			 \n\
			varying float v_FogWeighting; \n\
			 \n\
			void main(void) { \n\
				float CameraDistance = length((u_RelPosition + a_VertexPosition + u_CameraPosition).xz); \n\
				gl_Position = u_PMatrix * u_MVMatrix * vec4(a_VertexPosition, 1.0); \n\
				v_TextureCoord = a_TextureCoord; \n\
				 \n\
				v_LightWeighting = u_AmbientLight; \n\
				float FogDistance = CameraDistance; \n\
				if(u_FogEnabled) { \n\
					if(FogDistance < u_FogMinDistance \n\
							|| (FogDistance < u_FogMinDistance && u_FogMinDistance == u_FogMaxDistance)) { \n\
						v_FogWeighting = 0.0; \n\
					} else if(FogDistance > u_FogMaxDistance) { \n\
						v_FogWeighting = 1.0; \n\
					} else { \n\
						// v_FogWeighting = (FogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance); \n\
						// v_FogWeighting = sin(((FogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance)) * asin(1.0)); \n\
						v_FogWeighting = (exp((FogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance)) - 1.0) / (exp(1.0) - 1.0); \n\
					} \n\
				} else { \n\
					v_FogWeighting = 0.0; \n\
				} \n\
			}",
		fs: "precision mediump float; \n\
			 \n\
			varying vec2 v_TextureCoord; \n\
			varying vec3 v_LightWeighting; \n\
			varying float v_FogWeighting; \n\
			 \n\
			uniform sampler2D u_Sampler; \n\
			 \n\
			uniform bool u_FogEnabled; \n\
			uniform vec3 u_FogColor; \n\
			 \n\
			void main(void) { \n\
				vec4 textureColor = texture2D(u_Sampler, vec2(v_TextureCoord.s, v_TextureCoord.t)); \n\
				if(u_FogEnabled) { \n\
					gl_FragColor = vec4((1.0 - v_FogWeighting) * textureColor.rgb * v_LightWeighting + u_FogColor * v_FogWeighting, textureColor.a); \n\
				} else { \n\
					gl_FragColor = vec4(textureColor.rgb * v_LightWeighting, textureColor.a); \n\
				} \n\
			}",
		program: null
	},
	
	Sky: {
		vs: "precision mediump float; \n\
			 \n\
			attribute vec3 a_VertexPosition; \n\
			 \n\
			uniform mat4 u_MVMatrix; \n\
			uniform mat4 u_PMatrix; \n\
			 \n\
			uniform float u_SkyHorizon; \n\
			uniform float u_SkyTime; \n\
			 \n\
			void main(void) { \n\
				gl_Position = u_PMatrix * u_MVMatrix * vec4(a_VertexPosition, 1.0); \n\
			}",
		
		fs: "precision mediump float; \n\
			 \n\
			uniform vec3 u_SkyColor; \n\
			uniform vec3 u_SkyEndColor; \n\
			 \n\
			void main(void) { \n\
				gl_FragColor = vec4(u_SkyColor, 1.0); \n\
			}",
		program: null	
	}
};
