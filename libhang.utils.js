/*
 * LibHang.Utils - A set of useful functions for LibHang.
 *
 * Copyright 2013 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

var Utils = new Object();

Utils.buildGLProgram = function(gl, vertex_src, fragment_src) {
	var vertex=gl.createShader(gl.VERTEX_SHADER);
	var fragment=gl.createShader(gl.FRAGMENT_SHADER);
	
	gl.shaderSource(vertex, vertex_src);
	gl.shaderSource(fragment, fragment_src);
	
	gl.compileShader(vertex);
	if (!gl.getShaderParameter(vertex, gl.COMPILE_STATUS))
	 	throw "Failed to compile the Vertex Shader : "+gl.getShaderInfoLog(vertex);
	gl.compileShader(fragment);
	if (!gl.getShaderParameter(fragment, gl.COMPILE_STATUS))
	 	throw "Failed to compile the Fragment Shader : "+gl.getShaderInfoLog(fragment);
	
	var program=gl.createProgram();
	gl.attachShader(program, vertex);
	gl.attachShader(program, fragment);
	gl.linkProgram(program)
	
	 if(!gl.getProgramParameter(program, gl.LINK_STATUS))
	 	throw "Failed to link the Shader Program";
	 
	 return program;
}

Utils.deg2rad = function(deg) {
	return deg * Math.PI / 180;
}

Utils.rad2deg = function(rad) {
	return rad * 180 / Math.PI;
}

Utils.PushMatrix = function(matrix_array, matrix) {
	var copy = mat4.create();
	mat4.set(matrix, copy);
	matrix_array.push(copy);
}

Utils.PopMatrix = function(matrix_array) {
	return matrix_array.pop();
}

var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
	window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
