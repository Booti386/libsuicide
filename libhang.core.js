/*
 * LibHang - A 3D graphic renderer.
 *
 * Copyright 2013 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

// float r, float g, float b, [float a]
var Color = function(r, g, b, a) {
	this.r=r;
	this.g=g;
	this.b=b;
	this.a=a || 1.0;
}

Color.prototype.dup = function() {
        return new Color(this.r, this.g, this.b);
}
Color.prototype.set = function(r, g, b, a) {
        this.r=r;
        this.g=g;
        this.b=b;
	this.a=a || this.a;
        return this;
}
Color.prototype.toArray = function() {
        return [this.r, this.g, this.b];
}
Color.prototype.toArrayA = function() {
	return [this.r, this.g, this.b, this.a];
}

var TexturedCube = function(pos, diag, textures) {
	Cube.call(this, pos, diag);
	this.obj_name="TexturedCube";
	this.textures=typeof textures !== 'undefined' ? textures : new Array();
	this.shader_program=Shaders.Cube;
	this.buffers=Buffers.Cube;
}
extendClass(TexturedCube, Cube);

TexturedCube.prototype.dup = function(){
	return new TexturedCube(this.getPos(), this.getSize(), this.textures);
}

TexturedCube.prototype.setTextures = function(textures){
	this.textures=textures;
	return this;
}

// Point pos, Point angle
var Camera = function(pos, angle) {
	this.pos=pos;
	this.angle=angle;
}

Camera.prototype.dup = function() {
	return new Camera(this.pos, this.angle);
}
Camera.prototype.getPos = function() {
	return this.pos;
}
Camera.prototype.setPos = function(pos) {
	this.pos=pos;
	return this;
}
Camera.prototype.getAngle = function() {
	return this.angle;
}
Camera.prototype.setAngle = function(angle) {
	this.angle=angle;
	return this;
}

// HTML5Canvas canvas, World world, Camera camera
var RenderedWorld = function(canvas, world, camera) {
	this.canvas=canvas;
	this.world=world;
	this.camera=camera;
	
	this.objs=new Array();
	this.textures=new Array();
	
	this.aspect_ratio=1.0;

	this.ambient_light=new Color(1.0, 1.0, 1.0);

	this.fog_enabled=false;
	this.fog_min_distance=0.0;
	this.fog_max_distance=0.0;
	this.fog_color=new Color(1.0, 1.0, 1.0);

	this.sky_enabled=false;
	this.sky_horizon=0.5;
	this.sky_time=0.0;
	this.sky_color=new Color(0.0, 0.0, 1.0);
	this.sky_end_color=new Color(0.5, 0.0, 0.0);

	try {
		this.gl=canvas.getContext("webgl") || canvas.getContext("experimental-webgl", {depth:true, antialias:true});
	} catch(e) {
		throw "Failed to initialize WebGL : "+e;
	}
	if(!this.gl)
		throw "Failed to initialize WebGL";
	
	this.gl.viewport(0, 0, canvas.width, canvas.height);
	
	try {
		Shaders.Cube.program=Utils.buildGLProgram(this.gl, Shaders.Cube.vs, Shaders.Cube.fs);
		Buffers.Cube.initBuffers(this.gl);
	} catch(e) {
		throw "Failed to build the Cube Shader Program : "+e;
	}
	try {
		Shaders.Sky.program=Utils.buildGLProgram(this.gl, Shaders.Sky.vs, Shaders.Sky.fs);
		Buffers.Sky.initBuffers(this.gl);
	} catch(e) {
		throw "Failed to build the Sky Shader Program : "+e;
	}
	
	// Empty the screen.
	this.gl.clearColor(0.0, 0.0, 0.0, 1.0);
	this.gl.enable(this.gl.DEPTH_TEST);
	
	var self=this;
	requestAnimationFrame(function() {
		self.autoRender();
	});
}

RenderedWorld.prototype.getCamera = function() {
	return this.camera;
}
RenderedWorld.prototype.setCamera = function(camera) {
	this.camera=camera;
	return this;
}

RenderedWorld.prototype.add = function(obj) {
	this.gl.useProgram(obj.shader_program.program);
	
	obj.shader_program.vertexPositionAttribute = this.gl.getAttribLocation(obj.shader_program.program, "a_VertexPosition");
	obj.shader_program.textureCoordAttribute = this.gl.getAttribLocation(obj.shader_program.program, "a_TextureCoord");
	
	obj.shader_program.mvMatrixUniform = this.gl.getUniformLocation(obj.shader_program.program, "u_MVMatrix");
	obj.shader_program.pMatrixUniform = this.gl.getUniformLocation(obj.shader_program.program, "u_PMatrix");
	
	obj.shader_program.fogEnabledUniform = this.gl.getUniformLocation(obj.shader_program.program, "u_FogEnabled");
	obj.shader_program.fogMinDistanceUniform = this.gl.getUniformLocation(obj.shader_program.program, "u_FogMinDistance");
	obj.shader_program.fogMaxDistanceUniform = this.gl.getUniformLocation(obj.shader_program.program, "u_FogMaxDistance");
	obj.shader_program.fogColorUniform = this.gl.getUniformLocation(obj.shader_program.program, "u_FogColor");
	
	obj.shader_program.relPositionUniform = this.gl.getUniformLocation(obj.shader_program.program, "u_RelPosition");
	obj.shader_program.cameraPositionUniform = this.gl.getUniformLocation(obj.shader_program.program, "u_CameraPosition");
	
	obj.shader_program.samplerUniform = this.gl.getUniformLocation(obj.shader_program.program, "u_Sampler");
	obj.shader_program.ambientLightUniform = this.gl.getUniformLocation(obj.shader_program.program, "u_AmbientLight");
	
	if(!this.objs[obj.obj_name]) {
		this.objs[obj.obj_name]=new Array();
	}
	this.objs[obj.obj_name].push(obj);
	return this;
}

RenderedWorld.prototype.enableFullscreen = function() {
	this.canvas.width=window.screen.width;
	this.canvas.height=window.screen.height;
	this.updateViewport();
	this.canvas.requestFullscreen=this.canvas.requestFullscreen
		|| this.canvas.mozRequestFullScreen
		|| this.canvas.mozRequestFullscreen
		|| this.canvas.webkitRequestFullscreen;
	this.canvas.requestFullscreen();
	return this;
}
RenderedWorld.prototype.enableMouseLock = function() {
	this.canvas.requestPointerLock=this.canvas.requestPointerLock
		|| this.canvas.mozRequestPointerLock
		|| this.canvas.webkitRequestPointerLock;
	this.canvas.requestPointerLock();
	return this;
}

RenderedWorld.prototype.setBackColor = function(r, g, b, a) {
	this.gl.clearColor(r, g, b, a);
	this.gl.clear(this.gl.COLOR_BUFFER_BIT);
	return this;
}

RenderedWorld.prototype.setAmbientLight = function(r, g, b) {
	this.ambient_light.set(r, g, b);
	return this;
}

RenderedWorld.prototype.enableFog = function(enabled) {
	this.fog_enabled=enabled;
	return this;
}

RenderedWorld.prototype.setFogParams = function(min_distance, max_distance, color) {
	this.fog_min_distance=min_distance;
	this.fog_max_distance=max_distance;
	this.fog_color=color;
	return this;
}

RenderedWorld.prototype.enableSky = function(enabled) {
	this.gl.useProgram(Shaders.Sky.program);
	
	Shaders.Sky.vertexPositionAttribute = this.gl.getAttribLocation(Shaders.Sky.program, "a_VertexPosition");
	
	Shaders.Sky.mvMatrixUniform = this.gl.getUniformLocation(Shaders.Sky.program, "u_MVMatrix");
	Shaders.Sky.pMatrixUniform = this.gl.getUniformLocation(Shaders.Sky.program, "u_PMatrix");
	
	Shaders.Sky.skyHorizonUniform = this.gl.getUniformLocation(Shaders.Sky.program, "u_SkyHorizon");
	Shaders.Sky.skyTimeUniform = this.gl.getUniformLocation(Shaders.Sky.program, "u_SkyTime");
	Shaders.Sky.skyColorUniform = this.gl.getUniformLocation(Shaders.Sky.program, "u_SkyColor");
	Shaders.Sky.skyEndColorUniform = this.gl.getUniformLocation(Shaders.Sky.program, "u_SkyEndColor");
	
	this.sky_enabled=enabled;
	return this;
}

RenderedWorld.prototype.setSkyParams = function(horizon, time, color, end_color) {
	this.sky_horizon=horizon;
	this.sky_time=time;
	this.sky_color=color;
	this.sky_end_color=end_color;
	return this;
}

RenderedWorld.prototype.getViewport = function() {
	return new Vector(this.canvas.width, this.canvas.height, 0);
}
RenderedWorld.prototype.updateViewport = function() {
	this.gl.viewport(0, 0, this.canvas.width, this.canvas.height);
	this.gl.clear(this.gl.COLOR_BUFFER_BIT);
	return this;
}

RenderedWorld.prototype.getAspectRatio = function() {
	return this.aspect_ratio;
}
RenderedWorld.prototype.setAspectRatio = function(aspect_ratio) {
	this.aspect_ratio=aspect_ratio;
	return this;
}

RenderedWorld.prototype.loadImages = function(path /*, img1, img2, ...*/) {
	var handleLoadedTexture = function(gl, texture) {
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

		gl.bindTexture(gl.TEXTURE_2D, texture);
		
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
	
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);

		gl.bindTexture(gl.TEXTURE_2D, null);
	};
	
	for(var i=0;i+1<arguments.length;i++) {
		var idx=this.textures.push(this.gl.createTexture())-1;
		this.textures[idx].image=new Image();
		this.textures[idx].image.__libhang__idx=i;
		
		var self=this;
		this.textures[idx].image.onload=function() {
			handleLoadedTexture(self.gl, self.textures[this.__libhang__idx]);
		};
		this.textures[i].image.src=path + '/' + arguments[i+1];
        }
        return this;
}

var mvMatrix=mat4.create();
var mvMatrixes=new Array();
var pMatrix=mat4.create();

RenderedWorld.prototype.autoRender = function() {
	this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
	
	mat4.perspective(45, this.canvas.width / this.canvas.height, 0.1, 1000.0, pMatrix);
	mat4.identity(mvMatrix);

	if(this.sky_enabled) {
		this.gl.useProgram(Shaders.Sky.program);
		
		this.gl.bindBuffer(this.gl.ARRAY_BUFFER, Buffers.Sky.vertexPositionBuffer);
		this.gl.vertexAttribPointer(Shaders.Sky.vertexPositionAttribute, Buffers.Sky.vertexPositionBuffer.itemSize, this.gl.FLOAT, false, 0, 0);
		this.gl.enableVertexAttribArray(Shaders.Sky.vertexPositionAttribute);
	
		this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, Buffers.Sky.vertexIndexBuffer);
		
		this.gl.uniform1f(Shaders.Sky.skyHorizonUniform, this.sky_horizon);
		this.gl.uniform1f(Shaders.Sky.skyTimeUniform, this.sky_time);
		this.gl.uniform3fv(Shaders.Sky.skyColorUniform, this.sky_color.toArray());
		this.gl.uniform3fv(Shaders.Sky.skyEndColorUniform, this.sky_end_color.toArray());
		
		this.gl.uniformMatrix4fv(Shaders.Sky.mvMatrixUniform, false, mvMatrix);
		this.gl.uniformMatrix4fv(Shaders.Sky.pMatrixUniform, false, pMatrix);
		
		this.gl.drawElements(this.gl.TRIANGLES, Buffers.Sky.numItemsPerFace[0], this.gl.UNSIGNED_SHORT, Buffers.Sky.idxItemsPerFace[0]*2 /* sizeof(UNSIGNED_SHORT) */);
		this.gl.disableVertexAttribArray(Shaders.Sky.vertexPositionAttribute);
		this.gl.clear(this.gl.DEPTH_BUFFER_BIT);
	}
	
	mat4.rotate(mvMatrix, Utils.deg2rad(this.camera.getAngle().x), [1, 0, 0]);
	mat4.rotate(mvMatrix, Utils.deg2rad(this.camera.getAngle().y), [0, 1, 0]);
	mat4.rotate(mvMatrix, Utils.deg2rad(this.camera.getAngle().z), [0, 0, 1]);
	
	mat4.translate(mvMatrix, [this.camera.getPos().x, this.camera.getPos().y, this.camera.getPos().z]);
	
	for(var i in this.objs) {
		var objs=this.objs[i];
		var obj=objs[0];
		this.gl.useProgram(obj.shader_program.program);
		
		this.gl.uniform3fv(obj.shader_program.ambientLightUniform, this.ambient_light.toArray());
		
		this.gl.bindBuffer(this.gl.ARRAY_BUFFER, obj.buffers.vertexPositionBuffer);
		this.gl.vertexAttribPointer(obj.shader_program.vertexPositionAttribute, obj.buffers.vertexPositionBuffer.itemSize, this.gl.FLOAT, false, 0, 0);
		this.gl.enableVertexAttribArray(obj.shader_program.vertexPositionAttribute);

		this.gl.bindBuffer(this.gl.ARRAY_BUFFER, obj.buffers.vertexTextureCoordBuffer);
		this.gl.vertexAttribPointer(obj.shader_program.textureCoordAttribute, obj.buffers.vertexTextureCoordBuffer.itemSize, this.gl.FLOAT, false, 0, 0);
		this.gl.enableVertexAttribArray(obj.shader_program.textureCoordAttribute);
	
		this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, obj.buffers.vertexIndexBuffer);

		this.gl.uniform1i(obj.shader_program.fogEnabledUniform, this.fog_enabled);
		if(this.fog_enabled) {
			this.gl.uniform1f(obj.shader_program.fogMinDistanceUniform, this.fog_min_distance);
			this.gl.uniform1f(obj.shader_program.fogMaxDistanceUniform, this.fog_max_distance);
			this.gl.uniform3fv(obj.shader_program.fogColorUniform, this.fog_color.toArray());
		}
		
		this.gl.uniform3fv(obj.shader_program.cameraPositionUniform, this.camera.getPos().toArray());
		this.gl.uniform1i(obj.shader_program.samplerUniform, 0);
		this.gl.activeTexture(this.gl.TEXTURE0);
		
		for(var j=0;j < objs.length;j++) {
			var obj=objs[j];
			Utils.PushMatrix(mvMatrixes, mvMatrix);
			mat4.translate(mvMatrix, obj.getPos().toArray());
			
			this.gl.uniformMatrix4fv(obj.shader_program.mvMatrixUniform, false, mvMatrix);
			this.gl.uniformMatrix4fv(obj.shader_program.pMatrixUniform, false, pMatrix);
			
			this.gl.uniform3fv(obj.shader_program.relPositionUniform, obj.getPos().toArray());
			
			for(var k=0; k<obj.buffers.numFaces; k++) {
				this.gl.bindTexture(this.gl.TEXTURE_2D, this.textures[obj.textures[k]]);
				this.gl.drawElements(this.gl.TRIANGLES, obj.buffers.numItemsPerFace[k], this.gl.UNSIGNED_SHORT, obj.buffers.idxItemsPerFace[k]*2 /* sizeof(UNSIGNED_SHORT) */);
			}
			
			mvMatrix=Utils.PopMatrix(mvMatrixes);
		}
		this.gl.disableVertexAttribArray(obj.shader_program.vertexPositionAttribute);
		this.gl.disableVertexAttribArray(obj.shader_program.textureCoordAttribute);
	}
	
	var self=this;
	requestAnimationFrame(function() {
		self.autoRender();
	});
}
