var entity = new MovableEntity(new Point(0, 0, 0), new Vector(1, 1, 1));
document.write("Test 0 :");
document.write("<br>&emsp;var entity = new MovableEntity(new Point(0, 0, 0), new Vector(1, 1, 1))")
document.write("<br>&emsp;&emsp;Pos.x =", entity.getPos().x, ", Pos.y =", entity.getPos().y, ", Pos.z =", entity.getPos().z);
document.write("<br>&emsp;&emsp;Size.x=", entity.getSize().x, ", Size.y=", entity.getSize().y, ", Size.z=", entity.getSize().z);

entity.move(new Vector(-1, -1, -2));
document.write("<br><br>Test 1 :");
document.write("<br>&emsp;entity.move(new Vector(-1, -1, -2))");
document.write("<br>&emsp;&emsp;Pos.x =", entity.getPos().x, ", Pos.y =", entity.getPos().y, ", Pos.z =", entity.getPos().z);

entity.teleport(new Point(0, 1, 2).add(new Vector(3, 2, 1)));
document.write("<br><br>Test 2 :");
document.write("<br>&emsp;entity.teleport(new Point(0, 1, 2).add(new Vector(3, 2, 1)))");
document.write("<br>&emsp;&emsp;Pos.x =", entity.getPos().x, ", Pos.y =", entity.getPos().y, ", Pos.z =", entity.getPos().z);

entity.setSize(new Vector(4, 3, 2).add(new Vector(-3, -2, -1)));
document.write("<br><br>Test 3 :");
document.write("<br>&emsp;entity.setSize(new Vector(4, 3, 2).add(new Vector(-3, -2, -1)))");
document.write("<br>&emsp;&emsp;Size.x=", entity.getSize().x, ", Size.y=", entity.getSize().y, ", Size.z=", entity.getSize().z);

var test = new Cube(new Point(0, 0, 0), new Vector(2, 2, 2)).containsPoint(new Point(1, 2, 2));
document.write("<br><br>Test 4 :");
document.write("<br>&emsp;new Cube(new Point(0, 0, 0), new Vector(2, 2, 2)).containsPoint(new Point(1, 2, 2))");
document.write("<br>&emsp;&emsp;containsPoint : ", test ? "true":"false");

var test = new Cube(new Point(0, 0, 0), new Vector(3, 3, 3)).intersectsCube(new Cube(new Point(-1, 1, 0), new Vector(5, 1, 1)));
document.write("<br><br>Test 5 :");
document.write("<br>&emsp;new Cube(new Point(0, 0, 0), new Vector(3, 3, 3)).intersectsCube(new Cube(new Point(-1, 1, 0), new Vector(5, 1, 1)))");
document.write("<br>&emsp;&emsp;intersectsCube : ", test ? "true":"false");
