var LaunchInFullscreen = function() {
	try {
		var canvas=document.getElementById("libHangRender");
		canvas.width=window.screen.width;
		canvas.height=window.screen.height;
		canvas.requestFullscreen=canvas.requestFullscreen || canvas.mozRequestFullScreen || canvas.webkitRequestFullscreen;
			canvas.requestFullscreen();
		world=new RenderedWorld(new World(new Point(0, 0, 0), new Vector(500, 500, 500), GRAVITY_CONSTANTS.EARTH), canvas);
	} catch(e) {
		document.write("Unable to load libHang : ", e, ".");
	}
}
